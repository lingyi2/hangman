from turtle import *

#solution list
def letter_places(str, letter):
    places = []
    start = 0
    while True:
        where = str.find(letter,start)
        if (where == -1):
            break
        places.append(where)
        start = where + 1
    return places



def body():
# Draw a line
    forward(75)
    back(75)

# Go back to the center
    forward(25)

def head():
# Draw a circle for a head
    circle(25)
    right(90)

def right_arm():
# Draw right arm
    left(120)
    forward(60)
    back(60)

def left_arm():
# Draw left arm
    left(120)
    forward(60)
    back(60)
# Move down to bottom of line
    left(120)
    forward(50)

def left_leg():
# Left Leg
    right(30)
    forward(60)
    back(60)

def right_leg():
# Right leg
    left(60)
    forward(60)
    back(60)

solution = "hangman"
num_letters = len(solution)
in_progress = list( '_'*num_letters )
print(in_progress)
wrong_cont = 0
while True:

    letter = input("What is the letter you want to input? ")
    places = letter_places(solution, letter)
    if len(places) > 0:			# the guessed letter is at least one place in the solution
    	for place in places:
    		in_progress[place] = letter
    else:
        wrong_cont = wrong_cont + 1
        if (wrong_cont == 1):
            head()
        if (wrong_cont == 2):
            body()
        if (wrong_cont == 3):
            right_arm()
        if (wrong_cont == 4):
            left_arm()
        if (wrong_cont == 5):
            left_leg()
        if (wrong_cont == 6):
            right_leg()


    print(in_progress)
    print(wrong_cont)
    if (wrong_cont == 6):
        print("Game Over")
        break

# Wait for the user to close the window
done()
